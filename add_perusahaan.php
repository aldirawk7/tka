  <!-- Content Wrapper. Contains page content -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js" integrity="sha512-0XDfGxFliYJPFrideYOoxdgNIvrwGTLnmK20xZbCAvPfLGQMzHUsaqZK8ZoH+luXGRxTrS46+Aq400nCnAT0/w==" crossorigin="anonymous"></script>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default color-palette-bo">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-plus"></i>
             Tambah Perusahaan </h3>
          </div>
     
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <!-- form start -->
                <div class="box-body">

                  <!-- For Messages -->
                  <?php $this->load->view('admin/includes/_messages.php') ?>

				  <?php echo form_open(base_url('admin/dashboard/save_perusahaan'), 'class="form-horizontal"');  ?> 
    <!-- Buat tombol untuk menabah form data -->
   
    <div class="col-6">
<td>Nama Perusahaan :</td>
<input type="text" class="form-control" name="nama" required>
</div>

<div class="col-6">
        <td>Jenis PMA :</td>
        <select name="jenis_pma" class="form-control" required >
  <option value="">Jenis PMA</option>


  <option value="Perusahaan PMA">PMA</option>
  <option value="Perusahaan PMDN">PMDN</option>
  <option value="Kantor Perwakilan Badan Usaha Jasa Konstruksi (BUJKA)">Kantor Perwakilan Badan Usaha Jasa Konstruksi (BUJKA)</option>
  <option value="Kantor Perwakilan Perusahaan Perdagangan Asing (KP3A)">Kantor Perwakilan Perusahaan Perdagangan Asing (KP3A)</option>
  <option value="Kantor Perwakilan Perusahaan Asing (KPPA)">Kantor Perwakilan Perusahaan Asing (KPPA)</option>
  <option value="Yayasan">Yayasan</option>
  </select>
</div>

<div class="col-6">
<td>Nomor NIB :</td>
<input type="text" class="form-control" name="nib" required>
</div>
<div class="col-6">
<td>Bidang Usaha:</td>
<textarea type="text" class="form-control" name="usaha" required></textarea>
</div>

<div class="col-6">
<td>Lokasi:</td>
<input type="text" class="form-control" name="lokasi" required>
</div>

<div class="col-6">
<td>Nilai Investasi:</td>
<input type="text" class="form-control uang" name="nilai" required>
</div>
<div class="col-6">
<td>Tki :</td>
<input type="text" class="form-control" name="tki" required>
</div>


<br>
   
    <hr>
    <input type="submit" value="Simpan">
	<?php echo form_close(); ?>
				  <input type="hidden" id="jumlah-form" value="1">
                </div>
				
                <!-- /.box-body -->
              </div>
            </div>
          </div>  
        </div>
      </div>
    </section> 
  </div>

 
  <script type="text/javascript">
            $(document).ready(function(){

                // Format mata uang.
                $( '.uang' ).mask('000.000.000.000.000', {reverse: true});
			
            })
        </script>