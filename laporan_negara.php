
<style type="text/css">
	.kotak{
		-moz-border-radius: 10px; 
		-webkit-border-radius: 10px; 
		background-color: #f4f6f9; 
		border-radius: 10px; 
		border: 2px dashed #aaa; 
		width:550px;
		padding: 10px;

		margin-left:280px;
	}

</style>
<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/datatables/dataTables.bootstrap4.css"> 
<div class="content-wrapper">
<section class="content">
		<div class="card">
			<div class="card-header">
			<?php 
      $pagu=0; 
      $realisasi=0;
      ?>

     
     
   
 
      <div class="row">
          
          <!-- ./col -->
         
     
			<div class="card-body">
      <?php echo form_open(base_url('admin/dashboard/laporan_filter_negara'), 'class="form-horizontal"');  ?> 
      <?php if($date1 == '') {?>
    <div class="col-6">
<td>Dari:</td>
<input type="date" class="form-control" name="date1" required>
</div>
<div class="col-6">
<td>Sampai:</td>
<input type="date" class="form-control" name="date2" required>
</div>
<br>
<button type="submit" class="btn btn-primary">Button</button>
<?php } else { ?>
  <div class="col-6">
<td>Dari:</td>
<input type="date" class="form-control" name="date1" value="<?php echo $date1;?>" required>
</div>
<div class="col-6">
<td>Sampai:</td>
<input type="date" class="form-control" name="date2" value="<?php echo $date2; ?>" required>
</div>
<button type="submit">Button</button>
  <?php } ?>
<?php echo form_close(); ?>
			
         
                    <!-- ./col -->
          
	</section>
	<section class="content">
		<div class="card">
			<div class="card-header">
			<?php 
      $pagu=0; 
      $realisasi=0;
      ?>

     
     
   
 
      <div class="row">
          
          <!-- ./col -->
         
     
			<div class="card-body">
			<p>SEMUA</p>
			<br>
			<br>
      <?php	$printedSeasons = [] ?>
            <div class="row">
            <?php foreach($jumlah_tka as $p) { ?>
              <?php	if (!in_array($p['negara'], $printedSeasons)) {
						$printedSeasons[] = $p['negara']; ?>
            <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div style="margin-left:10px;" class="inner">
                <h6><b><?php echo $p['negara'] ?></b></h6>
             
                  <p><?php echo $p['jumlah_tka'] ?></p>

              </div>
              
          
            </div>
          </div>
       
              
          <?php } ?>
          <?php } ?>
                    <!-- ./col -->
          
	</section>



  
	<!-- /.content -->
</div>
<script src="<?= base_url()?>/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url()?>/assets/plugins/datatables/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });

</script> 
	<script>
		$("body").on("change",".tgl_checkbox",function(){
			$.post('<?=base_url("admin/admin_roles/change_status")?>',
			{
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',	
				id : $(this).data('id'),
				status : $(this).is(':checked') == true ? 1:0
			},
			function(data){
				$.notify("Status Changed Successfully", "success");
			});
		});

	</script>