  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default color-palette-bo">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-plus"></i>
             Tambah Surat Dukungan </h3>
          </div>
     
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <!-- form start -->
                <div class="box-body">

                  <!-- For Messages -->
                  <?php $this->load->view('admin/includes/_messages.php') ?>

				  <?php echo form_open(base_url('admin/dashboard/save'), 'class="form-horizontal"');  ?> 
    <!-- Buat tombol untuk menabah form data -->

   <div class="col-6">
        <td>Perusahaan :</td>
        <select name="perusahaan" class="form-control"  >
  <option value="">Pilih Perusahaaan</option>
  
  <?php foreach($perusahaaan as $p){ ?>
  <option value="<?php echo $p['id_perusahaan'];?>"><?php echo $p['nama_pt'];?></option>
  <?php } ?>
  </select>
</div>

<div class="col-6">
        <td>Tujuan :</td>
        <select name="tujuan" class="form-control"  >
  <option value="">Tujuan</option>
  

  <option value="Imigrasi">Imigrasi</option>
  <option value="Bina Penta">Imigrasi & BinaPenta</option>
  </select>
</div>
<div class="col-6">
<td>No surat :</td>
<input type="text" class="form-control" name="surat" required>
<?php if (is_array($pendukung) && count($pendukung) > 0) { ?>
<?php foreach($pendukung as $p){ ?>

      <input type="hidden" class="form-control" name="id_tka"  value="<?php echo ($p['id_surat'] + 1)?> " > 
   
      <?php } ?>
  <?php } else {?>
    <input type="hidden" class="form-control" name="id_tka"  value="1" > 
      <?php } ?>
    
</div>

<div class="col-6">
<td>tgl surat :</td>
<input type="date" class="form-control" name="date" required>
</div>

<div class="col-6">
<td>Alasan Tka:</td>
<textarea type="text" class="form-control" name="alasan" required></textarea>
</div>

<div class="col-6">
<td>Jumlah Tka:</td>
<input type="text" class="form-control" name="jumlah" required>
</div>

<div class="col-6">
<td>RPTKA :</td>
<input type="text" class="form-control" name="rptka" required>
</div>

<br>
    <button type="button" class ="btn btn-primary" id="btn-tambah-form">Tambah Data Tka</button>
    <button type="button" class ="btn btn-primary" id="btn-reset-form">Reset Tka</button><br><br>
    
    <b>TKA ke 1 :</b>
    <table>
    <tr>
       
        <td><input type="hidden" class="form-control" name="nis[]" required></td>
      </tr>
   
        <td>Nama TKA: </td>
        <td><input type="text" class="form-control" name="nama[]" required></td>
      </tr>
      <tr>
        <td>Passpor :</td>
        <td><input type="text"  class="form-control" name="telp[]" required></td>
    
      </tr>
      <tr>
      <td>Warga Negara : </td>
        <td>      <select name="negara[]" class="form-control"  >
  <option value="">Warga Negara</option>
  <?php foreach($negara as $p){ ?>

  <option value="<?php echo $p['id_negara'];?>"><?php echo $p['negara'];?></option>
  <?php } ?>
  </select></td></tr>
  <tr>
      <td>Jenis Tka : </td>
        <td>      <select name="jenis[]" class="form-control"  >
  <option value="">Jenis Tka</option>
  <?php foreach($jenis as $p){ ?>

  <option value="<?php echo $p['id_jenis_tka'];?>"><?php echo $p['jenis_tka'];?></option>
  <?php } ?>
  </select></td></tr>
  
      <tr>
        <td>Jabatan : </td>
        <td><input type="text" class="form-control" name="jabatan[]" required></td>
      </tr>

       
      </tr>
    </table>
    <br><br>
    <div id="insert-form"></div>
    
    <hr>
    <input type="submit" value="Simpan">
	<?php echo form_close(); ?>
				  <input type="hidden" id="jumlah-form" value="1">
                </div>
				
                <!-- /.box-body -->
              </div>
            </div>
          </div>  
        </div>
      </div>
    </section> 
  </div>

  
  <script>
  $(document).ready(function(){ // Ketika halaman sudah diload dan siap
    $("#btn-tambah-form").click(function(){ // Ketika tombol Tambah Data Form di klik
      var jumlah = parseInt($("#jumlah-form").val()); // Ambil jumlah data form pada textbox jumlah-form
      var nextform = jumlah + 1; // Tambah 1 untuk jumlah form nya
      
      // Kita akan menambahkan form dengan menggunakan append
      // pada sebuah tag div yg kita beri id insert-form
    // pada sebuah tag div yg kita beri id insert-form
	$("#insert-form").append("<b>TKA ke " + nextform + " :</b>" +
        "<table>" +
        "<tr>" +
  
        "<td><input type='hidden' class='form-control' name='nis[]' required></td>" +
        "</tr>" +
        "<tr>" +
        "<td>Nama Tka</td>" +
        "<td><input type='text' class='form-control' name='nama[]' required></td>" +
        "</tr>" +
        "<tr>" +
        "<td>Passpor</td>" +
        "<td><input type='text' name='telp[]' class='form-control' required></td>" +
        "</tr>" +
        "<tr>" +
        " <td>Warga Negara</td> <td>   <select name='negara[]' class='form-control'  >    <option value=''>Warga Negara</option><?php foreach($negara as $p){ ?><option value='<?php echo $p['id_negara'];?>'><?php echo $p['negara'];?></option><?php } ?></select></td>" +
        "</tr>" +
        "<tr>" +
        "<td>Jenis TKA</td> <td>   <select name='jenis[]' class='form-control'  >    <option value=''>Jenis TKA</option><?php foreach($jenis as $p){ ?><option value='<?php echo $p['id_jenis_tka'];?>'><?php echo $p['jenis_tka'];?></option><?php } ?></select></td>" +
        "</tr>" +
        "<tr>" +
        "<td>Jabatan</td>" +
        "<td><input type='text' name='jabatan[]' class='form-control' required></td>" +
        "</tr>" +
   
        "</table>" +
        "<br><br>");
      
      $("#jumlah-form").val(nextform); // Ubah value textbox jumlah-form dengan variabel nextform
    });
    // Buat fungsi untuk mereset form ke semula
    $("#btn-reset-form").click(function(){
      $("#insert-form").html(""); // Kita kosongkan isi dari div insert-form
      $("#jumlah-form").val("1"); // Ubah kembali value jumlah form menjadi 1
    });
  });
  </script>
