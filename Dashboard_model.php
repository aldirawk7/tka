<?php
	class Dashboard_model extends CI_Model{

		public function get_perusahaan($tahun){


			$query = $this->db->query('SELECT * FROM ci_perusahaan');
			return $query->result_array();
		}

		public function get_perusahaan_edit($id){


			$query = $this->db->query('SELECT * FROM ci_perusahaan WHERE id_perusahaan = '.$id.'');
			return $query->result_array();
		}

		public function get_jenis_tka_edit($id){


			$query = $this->db->query('SELECT * FROM ci_jenis_tka WHERE = '.$id.'');
			return $query->result_array();
		}

		public function get_jenis_tka($tahun){


			$query = $this->db->query('SELECT * FROM ci_jenis_tka  ');
			return $query->result_array();
		}
		public function get_pendukung($tahun){


			$query = $this->db->query('
			SELECT 
			count(c.id_tka) AS jumlah,
			a.id_surat AS a,
			a.no_surat,
			a.id_perusahaan,
			a.id_jenis,
			a.id_tka,
			a.tujuan,
			a.id_surat,
			a.tgl_entry,
			a.tgl_surat,
			a.alasan_tka,
			a.jumlah_tka,
			a.rptka,
			a.username,
			b.lokasi,
			b.bidang_usaha,
			b.nib,
			b.pma_pmd,
			b.nama_pt,
			b.alasan_tka,
			b.tki,
			b.nilai_investasi,
			b.id_perusahaan,
			b.tanggal
		  FROM
			ci_surat_dukungan a
			INNER JOIN ci_perusahaan b ON (a.id_perusahaan = b.id_perusahaan)
			INNER JOIN ci_tka c ON (a.id_tka = c.id_tka)
		  GROUP BY
			a,
			a.no_surat,
			a.id_perusahaan,
			a.id_jenis,
			a.id_tka,
			a.tujuan,
			a.id_surat,
			a.tgl_entry,
			a.tgl_surat,
			a.alasan_tka,
			a.jumlah_tka,
			a.rptka,
			a.username,
			b.lokasi,
			b.bidang_usaha,
			b.nib,
			b.pma_pmd,
			b.nama_pt,
			b.alasan_tka,
			b.tki,
			b.nilai_investasi,
			b.id_perusahaan,
			b.tanggal
		  
		  ORDER BY
			id_surat DESC
		  
			
');
			return $query->result_array();
		}

		public function get_pendukung_edit($id){


			$query = $this->db->query('
			SELECT *
			FROM ci_surat_dukungan
			INNER JOIN ci_perusahaan
			ON ci_perusahaan.id_perusahaan = ci_surat_dukungan.id_perusahaan
			
			WHERE ci_surat_dukungan.id_surat = '.$id.'
			ORDER by id_surat DESC
			
');
			return $query->result_array();
		}

		public function get_id_pendukung($tahun){


			$query = $this->db->query('
			SELECT *
			FROM ci_surat_dukungan
			INNER JOIN ci_perusahaan
			ON ci_perusahaan.id_perusahaan = ci_surat_dukungan.id_perusahaan
	
			ORDER BY ci_surat_dukungan.id_surat DESC
			limit 1
			
');
			return $query->result_array();
		}
		
		public function get_jumlah_tka($waktu1,$waktu2,$negara){
			if($waktu1 == ''){
				$where ='';
			} else {
				$where ="WHERE (b.tgl_entry BETWEEN '$waktu1' AND '$waktu2')";
			}
						

			$query = $this->db->query('
			SELECT 
			count(a.id_tka) AS jumlah_tka
		  FROM
			ci_tka a
			INNER JOIN ci_surat_dukungan b ON (b.id_tka = a.id_tka)
			'.$where.'
');
			return $query->result_array();
		}
		public function get_jumlah_dukungan($waktu1,$waktu2,$negara){
if($waktu1 == ''){
	$where ='';
} else {
	$where ="WHERE (b.tgl_entry BETWEEN '$waktu1' AND '$waktu2')";
}
			
			$query = $this->db->query('
			SELECT 
			count(b.id_surat) AS jumlah_dukungan
		
		  FROM
		ci_surat_dukungan b 
		'.$where.'
');


			return $query->result_array();
		}

		public function get_jumlah_dukungan_pma($waktu1,$waktu2,$negara){



			if($waktu1 == ''){
				$where ='';
			} else {
				$where ="WHERE (b.tgl_entry BETWEEN '$waktu1' AND '$waktu2')";
			}
						
			$query = $this->db->query('
			SELECT 
			count(b.id_surat) AS jumlah_dukungan,
			a.pma_pmd
		
		  FROM
		ci_surat_dukungan b 
		INNER JOIN ci_perusahaan a ON (b.id_perusahaan = a.id_perusahaan)
		'.$where.'
		GROUP BY a.pma_pmd

');
			return $query->result_array();
		}

		public function get_jumlah_tka_pma($waktu1,$waktu2,$negara){

			if($waktu1 == ''){
				$where ='';
			} else {
				$where ="WHERE (b.tgl_entry BETWEEN '$waktu1' AND '$waktu2')";
			}
						
			$query = $this->db->query('
			SELECT 
			count(a.id_tka) AS jumlah_tka,
			c.pma_pmd
			
		  FROM
			ci_tka a
			INNER JOIN ci_surat_dukungan b ON (b.id_tka = a.id_tka)
				INNER JOIN ci_perusahaan c ON (b.id_perusahaan = c.id_perusahaan)
				'.$where.'
				GROUP BY c.pma_pmd
			


');
			return $query->result_array();
		}

		public function get_jumlah_tki_pma($waktu1,$waktu2,$negara){
			if($waktu1 == ''){
				$where ='';
			} else {
				$where ="WHERE (tanggal BETWEEN '$waktu1' AND '$waktu2')";
			}

			$query = $this->db->query('
			SELECT 
			sum(a.tki) as jumlah_tki,
			a.pma_pmd
		  FROM
			ci_perusahaan a
			'.$where.'
		GROUP BY a.pma_pmd
		   
		  
');
			return $query->result_array();
		}

		public function get_nilai_investasi($waktu1,$waktu2,$negara){
			if($waktu1 == ''){
				$where ='';
			} else {
				$where ="WHERE (tanggal BETWEEN '$waktu1' AND '$waktu2')";
			}

			$query = $this->db->query('
			SELECT 
  sum(a.nilai_investasi) AS nilai
  
FROM
  ci_perusahaan a
'.$where.'
');
			return $query->result_array();
		}

		public function get_nilai_investasi_pma($waktu1,$waktu2,$negara){

			if($waktu1 == ''){
				$where ='';
			} else {
				$where ="WHERE (tanggal BETWEEN '$waktu1' AND '$waktu2')";
			}
			$query = $this->db->query('
			SELECT 
  sum(a.nilai_investasi) AS nilai,
  a.pma_pmd
  
FROM
  ci_perusahaan a
  '.$where.'
  GROUP BY a.pma_pmd

');
			return $query->result_array();
		}
		public function get_jumlah_tki($waktu1,$waktu2,$negara){

			if($waktu1 == ''){
				$where ='';
			} else {
				$where ="WHERE (tanggal BETWEEN '$waktu1' AND '$waktu2')";
			}
			$query = $this->db->query('
			SELECT 
			sum(a.tki) as jumlah_tki
		  FROM
			ci_perusahaan a
		   '.$where.'
		  
');
			return $query->result_array();
		}
	  
		public function get_pdf($id){


			$query = $this->db->query('
			SELECT *
			
			FROM ci_surat_dukungan
			INNER JOIN ci_perusahaan
			ON ci_perusahaan.id_perusahaan = ci_surat_dukungan.id_perusahaan
			
		
			WHERE ci_surat_dukungan.id_surat = '.$id.'
');
			return $query->result_array();
		}

		public function get_tka($id){


			$query = $this->db->query('
			SELECT *
			
			FROM ci_tka a
			
		INNER JOIN ci_negara b
		ON a.id_negara = b.id_negara
		INNER JOIN ci_jenis_tka c
		ON a.id_jenis = c.id_jenis_tka
			WHERE id_tka = '.$id.'
');
return $query->result_array();
}

public function get_tka_laporan($waktu1,$waktu2){

	if($waktu1 == ''){
		$where ='';
	} else {
		$where ="WHERE (tgl_entry BETWEEN '$waktu1' AND '$waktu2')";
	}
	$query = $this->db->query('
	SELECT 
  count(a.id_tka) AS jumlah_tka,

  b.id_negara,
  
  b.negara
FROM
  ci_tka a
  INNER JOIN ci_negara b ON (a.id_negara = b.id_negara)

  '.$where.'
GROUP BY
 
  b.id_negara,
   b.negara

');
return $query->result_array();
}
public function get_tka_jenis($id){


	$query = $this->db->query('
	SELECT c.jenis_tka
	
	FROM ci_tka a
	
INNER JOIN ci_negara b
ON a.id_negara = b.id_negara
INNER JOIN ci_jenis_tka c
ON a.id_jenis = c.id_jenis_tka
	WHERE id_tka = 1
');
			return $query->result_array();
		}

		public function get_tka_all(){


			$query = $this->db->query('
			SELECT *
			
			FROM ci_tka a
			
		INNER JOIN ci_negara b
		ON a.id_negara = b.id_negara
		
');
			return $query;
		}

		public function get_negara($tahun){


			$query = $this->db->query('SELECT * FROM ci_negara');
			return $query->result_array();
		}

		public function get_negara_edit($id){


			$query = $this->db->query('SELECT * FROM ci_negara WHERE id_negara = '.$id.'');
			return $query->result_array();
		}
		public function get_tenaga($tahun){


			$query = $this->db->query('SELECT * 
			FROM ci_tka  a INNER JOIN ci_perusahaan  b ON 
			a.id_tka = b.id_tka
			'
		
		);
			return $query->result_array();
		}

	
	}

?>
