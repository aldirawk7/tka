<?php defined('BASEPATH') OR exit('No direct script access allowed');

require('./application/libraries/PDFReport.php');


class Dashboard extends My_Controller {




		public function __construct(){
		
			parent::__construct();
			auth_check(); // check login auth
			function get_keys_for_duplicate_values($my_arr, $clean = false) {
				if ($clean) {
					return array_unique($my_arr);
				}
			
				$dups = $new_arr = array();
				foreach ($my_arr as $key => $val) {
				  if (!isset($new_arr[$val])) {
					 $new_arr[$val] = $key;
				  } else {
					if (isset($dups[$val])) {
					   $dups[$val][] = $key;
					} else {
					   $dups[$val] = array($key);
					   // Comment out the previous line, and uncomment the following line to
					   // include the initial key in the dups array.
					   // $dups[$val] = array($new_arr[$val], $key);
					}
				  }
				}
				return $dups;
			}
			function number($angka){
	
				$hasil_rupiah = number_format($angka,0,"",".");
				return $hasil_rupiah;
			 
			}
			function rupiah($angka){
	
	$hasil_rupiah = "" . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}
			function tgl_indo($tanggal){
				$bulan = array (
					1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
				$pecahkan = explode('-', $tanggal);
				
				// variabel pecahkan 0 = tanggal
				// variabel pecahkan 1 = bulan
				// variabel pecahkan 2 = tahun
			 
				return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
			}
			 
		
			
			$this->load->model('admin/dashboard_model', 'dashboard_model');
		
			
		}
	

	

	public function index(){

		$data['title'] = 'Dashboard';
		$tahun= 2021;
		$data['records'] = $this->dashboard_model->get_perusahaan($tahun);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/general', $data);

    	$this->load->view('admin/includes/_footer');

	}

	public function TA_index(){

		$data['title'] = 'Surat Pendukung';
		$tahun= 2021;
		$data['records'] = $this->dashboard_model->get_pendukung($tahun);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/ta_index', $data);

    	$this->load->view('admin/includes/_footer');

	}

	public function negara_index(){

		$data['title'] = 'Negara';
		$tahun= 2021;
		$data['records'] = $this->dashboard_model->get_negara($tahun);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/negara_index', $data);

    	$this->load->view('admin/includes/_footer');

	}

	public function add(){

		$data['title'] = '';
		$tahun= 2021;
		$data['records'] = $this->dashboard_model->get_tenaga($tahun);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/add', $data);

    	$this->load->view('admin/includes/_footer');

	}
	
	public function isian(){

		$data['title'] = 'Tenaga Ahli';
		$tahun= 2021;
		$data['perusahaaan'] = $this->dashboard_model->get_perusahaan($tahun);
		$data['jenis'] = $this->dashboard_model->get_jenis_tka($tahun);
		$data['pendukung'] = $this->dashboard_model->get_id_pendukung($tahun);
		$data['negara'] = $this->dashboard_model->get_negara($tahun);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/isian', $data);

    	$this->load->view('admin/includes/_footer');

	}

	public function add_perusahaan(){

		$data['title'] = 'tambah perusahaan';
		$tahun= 2021;
		$data['perusahaaan'] = $this->dashboard_model->get_perusahaan($tahun);
		$data['jenis'] = $this->dashboard_model->get_jenis_tka($tahun);
		$data['negara'] = $this->dashboard_model->get_negara($tahun);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/add_perusahaan', $data);

    	$this->load->view('admin/includes/_footer');

	}

	public function edit_perusahaanz($id){

		$data['title'] = 'edit perusahaan';
		$tahun= 2021;
		$data['perusahaaan'] = $this->dashboard_model->get_perusahaan_edit($id);
		$data['jenis'] = $this->dashboard_model->get_jenis_tka($id);
		$data['negara'] = $this->dashboard_model->get_negara($id);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/edit_perusahaan', $data);

    	$this->load->view('admin/includes/_footer');

	}

	public function add_negara(){

		$data['title'] = 'tambah perusahaan';
		$tahun= 2021;
		$data['perusahaaan'] = $this->dashboard_model->get_perusahaan($tahun);
		$data['jenis'] = $this->dashboard_model->get_jenis_tka($tahun);
		$data['negara'] = $this->dashboard_model->get_negara($tahun);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/add_negara', $data);

    	$this->load->view('admin/includes/_footer');

	}

	public function save(){
		// Ambil data yang dikirim dari form
		$perusahaan = $_POST['perusahaan']; // Ambil data nis dan masukkan ke variabel nis
		$jenis = $_POST['jenis']; // Ambil data nis dan masukkan ke variabel nis
		$tujuan= $_POST['tujuan']; // Ambil data nis dan masukkan ke variabel nis
		$alasan = $_POST['alasan']; // Ambil data nis dan masukkan ke variabel nis
		$jumlah = $_POST['jumlah']; // Ambil data nis dan masukkan ke variabel nis
		$tgl_surat= $_POST['date']; // Ambil data nis dan masukkan ke variabel nis
		$jabatan = $_POST['jabatan']; // Ambil data nis dan masukkan ke variabel nis
		$negara = $_POST['negara']; // Ambil data nis dan masukkan ke variabel nis
		$nis = $_POST['nis']; // Ambil data nis dan masukkan ke variabel nis\
		$surat = $_POST['surat']; // Ambil data nis dan masukkan ke variabel nis
		$nama = $_POST['nama']; // Ambil data nama dan masukkan ke variabel nama
		$rptka = $_POST['rptka']; // Ambil data telp dan masukkan ke variabel telp
		$telp = $_POST['telp']; // Ambil data telp dan masukkan ke variabel telp
		$id_tka = $_POST['id_tka']; // Ambil data telp dan masukkan ke variabel telp

		$data = array();
		$dataz = array();
		
		$index = 0; // Set index array awal dengan 0
		foreach($nis as $datanis){ // Kita buat perulangan berdasarkan nis sampai data terakhir
		  array_push($data, array(
			'id_tka'=>$id_tka,
			'id_jenis'=>$jenis[$index],
			'id_negara'=>$negara[$index],
			'tgl_entry'=> date('Y-m-d'), 
			'nama_tka'=>$nama[$index],  // Ambil dan set data nama sesuai index array dari $index
			'passpor'=>$telp[$index],  // Ambil dan set data telepon sesuai index array dari $index
			'jabatan'=>$jabatan[$index],  // Ambil dan set data alamat sesuai index array dari $index
		
		  ));
		  
		  $index++;
		}

	
			array_push($dataz, array(
		
			  'id_perusahaan'=>$perusahaan,  // Ambil dan set data nama sesuai index array dari $index
			  'no_surat'=>$surat,  // Ambil dan set data nama sesuai index array dari $index
			  'alasan_tka'=>$alasan,
			  'rptka'=>$rptka,  // Ambil dan set data nama sesuai index array dari $index
			  'jumlah_tka'=>$jumlah,  // Ambil dan set data nama sesuai index array dari $index
			  'tgl_surat'=>$tgl_surat,  // Ambil dan set data nama sesuai index array dari $index
			  'tgl_entry'=> date('Y-m-d'), // Ambil data nis dan masukkan ke variabel nis
			  'id_tka'=>$id_tka,  // Ambil dan set data nama sesuai index array dari $index
			  // Ambil dan set data telepon sesuai index array dari $index
			  'tujuan'=>$tujuan ,  // Ambil dan set data alamat sesuai index array dari $index $this->session->userdata('name');
			   'username'=> $this->session->userdata('username') ,
			));
			
		

	    
    $sqlz = $this->db->insert_batch('ci_tka', $data); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)
	$sql = $this->db->insert_batch('ci_surat_dukungan', $dataz); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)
	redirect('admin/dashboard/ta_index');
	 }


	 public function save_perusahaan(){
		// Ambil data yang dikirim dari form
		$perusahaan = $_POST['nama']; // Ambil data nis dan masukkan ke variabel nis
		$jenis = $_POST['jenis_pma']; // Ambil data nis dan masukkan ke variabel nis
		$nib= $_POST['nib']; // Ambil data nis dan masukkan ke variabel nis
		$tki = $_POST['tki']; // Ambil data nis dan masukkan ke variabel nis
		$usaha = $_POST['usaha']; // Ambil data nis dan masukkan ke variabel nis
		$tgl_entry= date('Y-m-d'); // Ambil data nis dan masukkan ke variabel nis
		$lokasi = $_POST['lokasi']; // Ambil data nis dan masukkan ke variabel nis
		$nilai = $_POST['nilai']; // Ambil data nis dan masukkan ke variabel nis
		$angka1= str_replace(".", "", $nilai);


		$dataz = array();
	
	
			array_push($dataz, array(
		
			  'nama_pt'=>$perusahaan,  // Ambil dan set data nama sesuai index array dari $index
			  'tki'=>$tki,  // Ambil dan set data nama sesuai index array dari $index
			  'pma_pmd'=>$jenis,  // Ambil dan set data nama sesuai index array dari $index
			  'nib'=>$nib,  // Ambil dan set data nama sesuai index array dari $index
		
			  'bidang_usaha'=>$usaha,  // Ambil dan set data nama sesuai index array dari $index
			  'tanggal'=>$tgl_entry,  // Ambil dan set data nama sesuai index array dari $index
			  'lokasi'=>$lokasi,  // Ambil dan set data nama sesuai index array dari $index
			  'nilai_investasi'=>$angka1 ,  // Ambil dan set data telepon sesuai index array dari $index
			  
			));
			
		

	    
  	$sql = $this->db->insert_batch('ci_perusahaan', $dataz); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)
	redirect('admin/dashboard');
	 }

	 public function edit_perusahaan($id){
		// Ambil data yang dikirim dari form
		$perusahaan = $_POST['nama']; // Ambil data nis dan masukkan ke variabel nis
		$jenis = $_POST['jenis_pma']; // Ambil data nis dan masukkan ke variabel nis
		$nib= $_POST['nib']; // Ambil data nis dan masukkan ke variabel nis

		$tki = $_POST['tki']; // Ambil data nis dan masukkan ke variabel nis
		$usaha = $_POST['usaha']; // Ambil data nis dan masukkan ke variabel nis
		$tgl_entry= date('Y-m-d'); // Ambil data nis dan masukkan ke variabel nis
		$lokasi = $_POST['lokasi']; // Ambil data nis dan masukkan ke variabel nis
		$nilai = $_POST['nilai']; // Ambil data nis dan masukkan ke variabel nis
	$angka1= str_replace(".", "", $nilai);


		$data = array(
			'nama_pt'=>$perusahaan,  // Ambil dan set data nama sesuai index array dari $index
			'tki'=>$tki,  // Ambil dan set data nama sesuai index array dari $index
			'pma_pmd'=>$jenis,  // Ambil dan set data nama sesuai index array dari $index
			'nib'=>$nib,  // Ambil dan set data nama sesuai index array dari $index
  
			'bidang_usaha'=>$usaha,  // Ambil dan set data nama sesuai index array dari $index
			'tanggal'=>$tgl_entry,  // Ambil dan set data nama sesuai index array dari $index
			'lokasi'=>$lokasi,  // Ambil dan set data nama sesuai index array dari $index
			'nilai_investasi'=>$angka1,  // Ambil dan set data telepon sesuai index array dari $index
			
		);
			
			
			$this->db->where('id_perusahaan', $id);
			$this->db->update('ci_perusahaan' , $data);
			redirect('admin/dashboard');
	    
  
	 }
	 public function update_negara($id){
		// Ambil data yang dikirim dari form
		$negara = $_POST['nama']; // Ambil data nis dan masukkan ke variabel nis
	
	


		$data = array(
			'negara'=>$negara,  // Ambil dan set data nama sesuai index array dari $index
			
			
		);
			
			
			$this->db->where('id_negara', $id);
			$this->db->update('ci_negara' , $data);
			redirect('admin/dashboard/negara_index');
	    
  
	 }

	 public function hapus_perusahaan($id){
		// Ambil data yang dikirim dari form
		$this->db->where('id_perusahaan',$id);
		$this->db->delete('ci_perusahaan');
			redirect('admin/dashboard');
	    
  
	 }
	 
	  public function hapus_negara($id){
		// Ambil data yang dikirim dari form
		$this->db->where('id_negara',$id);
		$this->db->delete('ci_negara');
			redirect('admin/dashboard/negara_index');
	    
  
	 }

	 public function delete_isian($idw,$ids){
		// Ambil data yang dikirim dari form
		$this->db->where('id_surat',$idw);
		$this->db->delete('ci_surat_dukungan');
		$this->db->where('id_tka',$ids);
		$this->db->delete('ci_tka');
			redirect('admin/dashboard/TA_index');
	    
  
	 }


	 public function save_negara(){
		// Ambil data yang dikirim dari form
		$negara = $_POST['nama']; // Ambil data nis dan masukkan ke variabel nis
		

		$dataz = array();
	
	
			array_push($dataz, array(
		
			  'negara'=>$negara // Ambil dan set data nama sesuai index array dari $index
			
			));
			
		

	    
  	$sql = $this->db->insert_batch('ci_negara', $dataz); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)
	redirect('admin/dashboard/negara_index');
	 }

	 	public function laporan_negara(){

				$data['title'] = ' BERDASARKAN PMA';
				$tahun= 2021;
			
				$waktu1= '';
				$waktu2= '';
				$data['date1'] = $waktu1;
					$data['date2'] = $waktu2;
				$negara= 1;
				$data['jumlah_tka'] = $this->dashboard_model->get_tka_laporan($waktu1,$waktu2);
			
			
				$this->load->view('admin/includes/_header');
				
				$this->load->view('admin/dashboard/laporan_negara', $data);
				
				$this->load->view('admin/includes/_footer');
				
				}

				
	public function laporan(){

			$data['title'] = ' BERDASARKAN PMA';
			$tahun= 2021;
		
			$waktu1= '';
			$waktu2= '';
			$data['date1'] = $waktu1;
				$data['date2'] = $waktu2;
			$negara= 1;
			$data['jumlah_jenis'] = $this->dashboard_model->get_jumlah_jenis($waktu1,$waktu2,$negara);
			$data['jumlah_tka'] = $this->dashboard_model->get_jumlah_tka($waktu1,$waktu2,$negara);
			$data['jumlah_tki'] = $this->dashboard_model->get_jumlah_tki($waktu1,$waktu2,$negara);
			$data['jumlah_dukungan'] = $this->dashboard_model->get_jumlah_dukungan($waktu1,$waktu2,$negara);
			$data['nilai'] = $this->dashboard_model->get_nilai_investasi($waktu1,$waktu2,$negara);
			$data['nilai_pma'] = $this->dashboard_model->get_nilai_investasi_pma($waktu1,$waktu2,$negara);
			$data['tka_pma'] = $this->dashboard_model->get_jumlah_tka_pma($waktu1,$waktu2,$negara);
			$data['tki_pma'] = $this->dashboard_model->get_jumlah_tki_pma($waktu1,$waktu2,$negara);
			$data['dukungan_pma'] = $this->dashboard_model->get_jumlah_dukungan_pma($waktu1,$waktu2,$negara);
		
			$this->load->view('admin/includes/_header');
			
			$this->load->view('admin/dashboard/laporan', $data);
			
			$this->load->view('admin/includes/_footer');
			
			}

			public function laporan_filter(){

				$data['title'] = ' BERDASARKAN PMA';
				$tahun= 2021;
			
				$waktu1= $_POST['date1'];
				$waktu2= $_POST['date2'];
				$data['date1'] = $waktu1;
				$data['date2'] = $waktu2;
				$negara= 1;
				$data['jumlah_jenis'] = $this->dashboard_model->get_jumlah_jenis($waktu1,$waktu2,$negara);
				$data['jumlah_tka'] = $this->dashboard_model->get_jumlah_tka($waktu1,$waktu2,$negara);
				$data['jumlah_tki'] = $this->dashboard_model->get_jumlah_tki($waktu1,$waktu2,$negara);
				$data['jumlah_dukungan'] = $this->dashboard_model->get_jumlah_dukungan($waktu1,$waktu2,$negara);
				$data['nilai'] = $this->dashboard_model->get_nilai_investasi($waktu1,$waktu2,$negara);
				$data['nilai_pma'] = $this->dashboard_model->get_nilai_investasi_pma($waktu1,$waktu2,$negara);
				$data['tka_pma'] = $this->dashboard_model->get_jumlah_tka_pma($waktu1,$waktu2,$negara);
				$data['tki_pma'] = $this->dashboard_model->get_jumlah_tki_pma($waktu1,$waktu2,$negara);
				$data['dukungan_pma'] = $this->dashboard_model->get_jumlah_dukungan_pma($waktu1,$waktu2,$negara);
			
				$this->load->view('admin/includes/_header');
				
				$this->load->view('admin/dashboard/laporan', $data);
				
				$this->load->view('admin/includes/_footer');
				
				}


				public function laporan_filter_negara(){

					$data['title'] = ' BERDASARKAN PMA';
					$tahun= 2021;
				
					$waktu1= $_POST['date1'];
					$waktu2= $_POST['date2'];
					$data['date1'] = $waktu1;
					$data['date2'] = $waktu2;
					$negara= 1;
					$data['jumlah_tka'] = $this->dashboard_model->get_tka_laporan($waktu1,$waktu2);
				
					$this->load->view('admin/includes/_header');
					
					$this->load->view('admin/dashboard/laporan_negara', $data);
					
					$this->load->view('admin/includes/_footer');
					
					}
	 	function pdf_table()
	{
		ob_start();
		$this->load->library('PDFReport');
		$pdf = new PDFReport();
		$pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false);
		// $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false);
		# START deklarasi header
		unset($dataHeader);
		$id= $this->uri->segment(4);
		$id_tka= $this->uri->segment(5);
		$data= $this->dashboard_model->get_pdf($id);
		$dataz= $this->dashboard_model->get_tka($id_tka);
		$dataHeader = [];
		$pdf->dataHeader = $dataHeader;
		$pdf->RepFuncHeader = "";
		# END deklarasi header
		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		// set margins
		$pdf->SetMargins(12, 20, 12);

    // use the font
       
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->setPrintFooter(false);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetTitle('Laporan Konsumen');
		$pdf->setFooterMargin(30);
		$pdf->SetAutoPageBreak(true, 35);
		$pdf->SetAuthor('The AdaMz');
		$pdf->SetDisplayMode('fullwidth', 'continuous');

		$pdf->SetFont($font_family = 'calibri', $font_variant = 'calibriz', $fontsize = 12);


		# Ambil data
	
		$pdf->AddPage();

		$no = 1;
		foreach($dataz as $p) {
		$jumlah =	count($dataz);

		}
		if ($jumlah >= 3) {
		$table_html = '
		<style>
		ol {
		  display: block;
		  list-style-type: decimal;
		  margin-top: 1em;
		  margin-bottom: 1em;
		  margin-left: 0;
		  margin-right: 0;
		  padding-left: 20px;
		}
		</style>
			<p>&nbsp;</p>';
			foreach($data as $p) {
				$table_html .='
						
					
						<p style="font-family:calibriz;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lampiran </p>
						<br>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nomor&nbsp; : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/A.1/2021</li>
					<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanggal : </li>
					<p>&nbsp;</p>
					<p style="text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Daftar';
								$i = 0;
								$len = count($dataz);
								$printedSeasons = [];
							
							
								foreach($dataz as $key => $b) {
								
									if (!in_array($b['jenis_tka'], $printedSeasons)) {
										$printedSeasons[] = $b['jenis_tka'];
									
											if ($key === array_key_first($dataz)) {
											
													$table_html .='
													'.$b['jenis_tka'].'';
						
												
											}  else if ($key === array_key_last($dataz)){
												$table_html .='
												dan '.$b['jenis_tka'].'';
										
											} else {
												if ($len > 2){
													$table_html .='
													 '.$b['jenis_tka'].'dan ';
													} else if ($len < 1) {
														$table_html .='
														'.$b['jenis_tka'].'';
													}
												}
											$i++;
							
								
										}	
								
								}
					'
					'; 
							}
					$table_html .=' '.$p['nama_pt'].'
					<ol>
					<table style="width:550;"  border="1">
					
						<tr>
						  <th style="text-align:center; width:40px">No</th>
						  <th style="text-align:center; width:120px">Nama</th>
						  <th style="text-align:center">No Passpor</th>
						  <th style="text-align:center">Warga Negara</th>
						  
						  <th style="text-align:center" >
						  ';
						  $printedSeasons = [];
						  foreach($dataz as $p) {
							if (!in_array($p['jenis'], $printedSeasons)) {
								$printedSeasons[] = $p['jenis'];
							if($p['id_jenis'] == '3') {
						  $table_html .= '
						  Keterangan
						  ';
							} else if ($p['id_jenis'] == '2') {
								$table_html .= '
								Keterangan
								';
							
							} else {
								$table_html .= '
								Jabatan
								';
							}
						}
						}
						  $table_html .= '
						  </th>
						 
					
						</tr>
						';
						$no=1;
						$sum = 0;
						foreach($dataz as $p) {
					
						$table_html .='
						
						<tr>
						  <td style="text-align:center">'.$no++.'</td>
						  <td style="text-align:left"> '.$p['nama_tka'].'</td>
						  <td style="text-align:center">'.$p['passpor'].'</td>
						  <td style="text-align:center">'.$p['negara'].'</td>
						  <td style="text-align:center">'.$p['jabatan'].'</td>
						 
						</tr>
				';
				
						}
					}
					
				
					$table_html .='
				</table>
						
					
						
				
				';
		
		$pdf->writeHTML($table_html, true, false, false, false, '');

		ob_clean();
		$pdf->Output('Laporan Konsumen_' . date('YmdHis') . '.pdf', 'I');
	}

	function pdf()
	{
		ob_start();
		$this->load->library('PDFReport');
		$pdf = new PDFReport();
		$pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false);
		// $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false);
		# START deklarasi header
		unset($dataHeader);
		$id= $this->uri->segment(4);
		$id_tka= $this->uri->segment(5);
		$data= $this->dashboard_model->get_pdf($id);
		$dataz= $this->dashboard_model->get_tka($id_tka);
		$dataHeader = [];
		$pdf->dataHeader = $dataHeader;
		$pdf->RepFuncHeader = "";
		# END deklarasi header
		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		// set margins
		$pdf->SetMargins(12, 20, 12);

    // use the font
       
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->setPrintFooter(false);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetTitle('Laporan Konsumen');
		$pdf->setFooterMargin(30);
		
		$pdf->SetAuthor('The AdaMz');
		$pdf->SetDisplayMode('fullwidth', 'continuous');

		$pdf->SetFont($font_family = 'calibri', $font_variant = 'calibriz', $fontsize = 12);


		# Ambil data
	
		$pdf->AddPage();

		$no = 1;
		foreach($dataz as $p) {
		$jumlah =	count($dataz);

		}
		if ($jumlah <= 3) {
		$table_html = '
		<style>
		ol {
		  display: block;
		  list-style-type: decimal;
		  margin-top: 1em;
		  margin-bottom: 1em;
		  margin-left: 0;
		  margin-right: 0;
		  padding-left: 20px;
		}
		</style>
		<p>&nbsp;</p>';
	
			foreach($data as $p) {
			$table_html .= '<p style="text-align:right">'.$p['username'].'</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
			<table  style="padding-left:10px;padding-right:10px;" border="1">
		<tbody>
		<tr>
		<td >
	
		<p>Yth. Bapak Deputi Bidang Pengembangan Iklim PM</p>
		<p>&nbsp;</p>
	
		<p style="text-align:justify; margin-left:20px;">Terlampir kami sampaikan draft jawaban surat kepada ';
		foreach($data as $p) {
			if($p['tujuan'] == 'Imigrasi'){
			
		$table_html.='
		 Direktur Jenderal Imigrasi, Kementerian
		Hukum dan HAM';
			} else {
				
				$table_html.='
		Direktur Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja, Kementerian
		Ketenagakerjaan dan Direktur Jenderal Imigrasi, Kementerian
		Hukum dan HAM';
			}
		}
		$table_html .=' untuk permohonan masuk ke Indonesia bagi'; 
		$i = 0;
		$len = count($dataz);
		$printedSeasons = [];
	
	
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}

		
		$table_html .='
		yang di ajukan  '.$p['nama_pt'].'.</p>
		<p>&nbsp;</p>
		<p>Mohon koreksi dan arahan lebih lanjut</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		
	
		<p>Terima kasih dan salam hormat,</p>
		<p >Suhartono</p>
		<p>&nbsp;</p>
	
		</td>
		</tr>
		</tbody>
		</table>
			<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
				<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>		<p>&nbsp;</p>
		<p>&nbsp;</p>
<BR>
<BR>
		<table width="0">
			<tbody>
				<tr>
					<td width="78">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nomor</p>
					</td>
					<td width="19">
						<p>:</p>
					</td>
					<td width="230">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/A.1/2021</p>
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="150">
						<p>Jakarta,</p>
					</td>
				</tr>
				<tr>
					<td width="78">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lampiran</p>
					</td>
					<td width="19">
						<p>:</p>
					</td>
					<td width="300">
						<p>-</p>
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="195">
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td width="78">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Perihal</p>
					</td>
					<td width="19">
						<p>:</p>
					</td>
					<td width="200">
					<p>Permohonan Masuk ke Indonesia untuk 
						'; 
						$i = 0;
						$len = count($dataz);
						$printedSeasons = [];
					
					
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}
				
				$table_html .='
				'.$p['nama_pt'].'
				';
						$table_html .='</p>
					</td>
					<td width="40">
						<p><strong>&nbsp;</strong></p>
					</td>
					<td width="195">
						<p><strong>&nbsp;</strong></p>
					</td>
				</tr>
				<tr>
					<td width="78">&nbsp;</td>
					<td width="19">
						<p>&nbsp;</p>
					</td>
					<td width="300">
						<p>&nbsp;</p>
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="195">
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td colspan="3" width="397">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kepada Yth.</p>
						
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="195">
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td colspan="5" width="632">
					';
					foreach($data as $p) {
						if($p['tujuan'] == 'Imigrasi'){
							$pdf->SetAutoPageBreak(true, 39);
						$table_html .='
						<p style="text-align: justify;  margin-left: 0; font-family:calibriz; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Direktur Jenderal Imigrasi, Kementerian
						Hukum dan HAM</p><br>
						
				
				
					';
		
		} else {
			$pdf->SetAutoPageBreak(true, 33);
					$table_html .='
						<ol style="text-align: justify;  margin-left: 0; font-family:calibriz;">
						
				<li style="text-align: justify;  margin-left: 0; font-family:calibriz; ">Direktur Jenderal Imigrasi, Kementerian
				Hukum dan HAM</li>
				<li style="text-align: justify; font-family:calibriz;">Direktur Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja,<br>Kementerian
				Ketenagakerjaan.</li><br>
			</ol>
			';
		}
	}	
		$table_html .='
					</td>
				</tr>
				<tr>
					<td colspan="4" width="632">
						
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;di - &nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jakarta	<br></p>
					</td>
				</tr>
			
		</table>
	
		<table style="padding-left:20px;padding-right:26px;">
		<tr>
		<td width="568">
		<p style="text-align: justify;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan surat dari '.$p['nama_pt'].' '.$p['no_surat'].'';
		foreach($data as $p) {
			if($p['tgl_surat'] == '') {$table_html.='';
			
			} else {
				$table_html .= '
				tanggal '.tgl_indo($p['tgl_surat']).'
				';
			}
		}
		$table_html.='
		perihal sebagaimana tersebut pada pokok surat, dengan ini kami sampaikan hal-hal sebagai berikut:</p>
		</td>
	</tr>

		  </table>

		<ol style="text-align: justify;  margin-left: 0; ">
	
			<li style="text-align: justify;  margin-left: 0; ">'.$p['nama_pt'].' merupakan  '.$p['pma_pmd'].' dengan NIB '.$p['nib'].' bergerak di bidang usaha
			'.$p['bidang_usaha'].' yang berlokasi di '.$p['lokasi'].' dengan ';
			foreach($data as $p) {
				if($p['pma_pmd'] == 'Perusahaan PMA' | $p['pma_pmd'] == 'Perusahaan PMDN'){
			$table_html.='
			total investasi sebesar Rp.';
			if ($p["nilai_investasi"]  == 0) { 
				$table_html.='
				'.$p["nilai_investasi"].' ';
			} else if ($p["nilai_investasi"]  < 1000000000) { 
				$table_html.=''.rupiah($p["nilai_investasi"]/1000000).' juta ';
									} else if ($p["nilai_investasi"]  < 1000000000000) { 
							$table_html.=''.rupiah($p["nilai_investasi"]/1000000000).' Miliar ';
				} else if ($p["nilai_investasi"] < 1000000000000000) { 
					$table_html.=''.rupiah($p["nilai_investasi"]/1000000000000).' Triliun ';
							 } 
				 $table_html.='dan
			';
		
				} else {

					$table_html.='
					';
				
				}
			
			}
			
			
			$table_html .='
				menyerap Tenaga Kerja Indonesia sebanyak '.number($p['tki']).' orang.</li><br>
			<li style="text-align: justify;">Dalam rangka '.$p['alasan_tka'].', '.$p['nama_pt'].'
				berencana akan mendatangkan '.$p['jumlah_tka'].'';
				
		if($p['rptka'] == ''){
							$table_html .=''.$p['rptka'].'';
		} else {
							$table_html .=' '.$p['rptka'].'';
		}
				
				
				$table_html .=', dengan data sebagai berikut :</li>
		</ol>
		<br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;
		<table style="width:536px"  border="1">
	
		<tr>
		  <th style="text-align:center; width:40px">No </th>
		  <th style="text-align:center; width:120px">Nama</th>
		  <th style="text-align:center">No Passpor</th>
		  <th style="text-align:center">Warga Negara</th>
		  
		  <th style="text-align:center" >
		  ';
		  $printedSeasons = [];
						  foreach($dataz as $p) {
							if (!in_array($p['jenis'], $printedSeasons)) {
								$printedSeasons[] = $p['jenis'];
							if($p['id_jenis'] == '3') {
						  $table_html .= '
						  Keterangan
						  ';
							} else if ($p['id_jenis'] == '2') {
								$table_html .= '
								Keterangan
								';
							} else if ($p['id_jenis'] == '5') {
								$table_html .= '
								Keterangan
								';
							} else {
								$table_html .= '
								Jabatan
								';
							}
						}
						}
		  $table_html .= '
		  </th>
		 
	
		</tr>
		';
		$no=1;
		$sum = 0;
		foreach($dataz as $p) {
	
		$table_html .='
		
		<tr>
		  <td style="text-align:center">'.$no++.'</td>
		  <td style="text-align:left"> '.$p['nama_tka'].'</td>
		  <td style="text-align:center">'.$p['passpor'].'</td>
		  <td style="text-align:center">'.$p['negara'].'</td>
		  <td style="text-align:center">'.$p['jabatan'].'</td>
		 
		</tr>
';

		}
		foreach($data as $p) {
$table_html .='
</table>
		
		<ol start="3">
			<li style="text-align: justify;">Mengacu pada Surat Edaran Satuan Tugas Penanganan Covid-19 Nomor 8 Tahun 2021
				tentang Protokol Kesehatan Perjalanan Internasional Pada Masa Pandemi <em >Corona Virus Disease</em> 2019
				(Covid-19) pada butir F angka 2 point a,  ';
				foreach($data as $p) {
					if($p['pma_pmd'] == 'Yayasan') {
						$table_html.='Yayasan';
			
					} else {
					$table_html .= 'Perusahaan';
					}
				}
				$table_html.='
				yang akan mendatangkan';
				
				$i = 0;
		$len = count($dataz);
		$printedSeasons = [];
	
	
		
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}

				
				$table_html .=' telah memenuhi ketentuan
				dalam Peraturan Menteri Hukum dan HAM Nomor 26 Tahun 2020 tentang Visa dan Izin Tinggal Dalam Masa Adaptasi
				Kebiasaan Baru. ';
				foreach($data as $p) {
					if($p['pma_pmd'] == 'Yayasan') {
						$table_html.='Yayasan';
			
					} else {
					$table_html .= 'Perusahaan';
					}
				}
				$table_html.='
				bersedia untuk memenuhi segala persyaratan dan melakukan RT-PCR Covid-19, baik sebelum kedatangan ke Indonesia
				maupun masuk ke Indonesia dan membiayai karantina mandiri dan/atau perawatan di rumah sakit serta mengikuti
				seluruh protokol kesehatan yang ditetapkan oleh pemerintah'; 
				$i = 0;
				$len = count($dataz);
				$printedSeasons = [];
			
				$lena = count($dataz);
				
				
				foreach($dataz as $key => $p) {
				
					if (!in_array($p['negara'], $printedSeasons)) {
						$printedSeasons[] = $p['negara'];
					
				
					if ($p['id_negara'] == 1 | $p['id_negara'] == 2 | $p['id_negara'] == 3 ){
						$jumlah =	count($p);

					
					
					
						if ($i === 1) {
							$table_html.='
							dengan memperhatikan perjanjian bilateral Travel Corridor Arrangement (TCA) antara Indonesia dan  
							
							'; 
					
						}
					} else{

						if ($i === 0) {
							$table_html.='
							dengan memperhatikan perjanjian bilateral Travel Corridor Arrangement (TCA) antara Indonesia dan  
							
							'; 
					
					}
				
						$i++;
						
					
						
			
					}
				if($p['id_negara'] == 1 ){
					if ($key === array_key_first($dataz)) {
						$table_html .=' '.$p['negara'].'';
					}  else if ($key === array_key_last($dataz)){
						$table_html .=', '.$p['negara'].'';

					} else {

						$table_html .=', '.$p['negara'].'';
					}

					
			
						
					
					// …
					
	
					
			
			
				
			
				}
				else if($p['id_negara'] == 2 ){
					if ($key === array_key_first($dataz)) {
						$table_html .=' '.$p['negara'].'';
					}  else if ($key === array_key_last($dataz)){
						$table_html .=', '.$p['negara'].'';

					} else {

						$table_html .=', '.$p['negara'].'';
					}

					
			
						
					
					}
					else if($p['id_negara'] == 3 ){
						if ($key === array_key_first($dataz)) {
							$table_html .=' '.$p['negara'].'';
						}  else if ($key === array_key_last($dataz)){
							$table_html .=', '.$p['negara'].'';
	
						} else {
	
							$table_html .=', '.$p['negara'].'';
						}
	
					
		
						
						}
						// else if($p['id_negara'] == 4 ){
						// 	if ($key === array_key_first($dataz)) {
						// 		$table_html .=' '.$p['negara'].',';
						// 	}  else if ($key === array_key_last($dataz)){
						// 		$table_html .=' '.$p['negara'].'';
		
						// 	} else {
		
						// 		$table_html .=' '.$p['negara'].',';
						// 	}
		
						
			
							
						// 	}

							$i++;
			
					
					
			
										} 
					
				}
				
				$table_html .='. </li><br>
			<li style="text-align: justify;">Berdasarkan hal tersebut di atas, kami mohon kiranya';
			
			foreach($data as $p) {
				if($p['tujuan'] == 'Imigrasi'){
				$table_html .='
				Direktorat Jenderal Imigrasi, Kementerian Hukum dan HAM berkenan
				memberikan dukungan atas permohonan Izin masuk ke Indonesia bagi ';
				$i = 0;
				$len = count($dataz);
				$printedSeasons = [];
			
			
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}
		
			
		
			

} else {

			$table_html .='
			Direktorat Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja, Kementerian Ketenagakerjaan berkenan menerbitkan '; 
			if($p['rptka']== ''){
				$table_html.='RPTKA dan';
			} else {
				$table_html.='';
			}
		
			$table_html.=' notifikasi penggunaan TKA serta Direktorat Jenderal Imigrasi, Kementerian Hukum dan HAM berkenan
			memberikan dukungan atas permohonan Izin masuk ke Indonesia bagi ';
					$i = 0;
		$len = count($dataz);
		$printedSeasons = [];
	
	
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}

			
}
				}


			
		$table_html.' 
					
	';	
			$table_html.=' yang akan diajukan oleh '.$p['nama_pt'].' sesuai dengan Peraturan Perundang-undangan yang berlaku.</li>
					
	</ol>

	<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Demikian atas perhatian dan kerjasama yang baik, disampaikan terima kasih.</p>
		<p></p>
		<p style="text-align: center;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
			a.n. Menteri Investasi / Kepala Badan Koordinasi Penanaman Modal 
			<br>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;  
			Deputi Bidang Pengembangan Iklim Penanaman Modal,</p>

		<p style="text-align: center;">&nbsp;</p>

		<p></p>
		<p style="text-align: center;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			Yuliot</p>
		<p></p>
		<p>  &nbsp; &nbsp; &nbsp; Tembusan Yth. :</p>
			<ol>
			<li>Menteri Investasi / Kepala Badan Koordinasi Penanaman Modal;</li>
		
			';
		
				$table_html .='
			
		
		
			';
				} 
				foreach ($data as $p) {
					if($p['tujuan'] == 'Imigrasi'){
				$table_html .='
				<li style="text-align:justify">Direktur Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja, Kementerian
				Ketenagakerjaan;</li>
				';
				
				} else {
					$table_html .='
					';
				}
					
	

				
				$table_html .='
				<li>Direktur Jenderal Pembinaan Pengawasan Ketenagakerjaan dan Keselamatan dan Kesehatan Kerja, Kementerian
				Ketenagakerjaan;</li>
			<li>Sekretaris Utama, Kementerian Investasi/BKPM;</li>
			<li>Deputi Bidang Pelayanan Penanaman Modal, Kementerian Investasi/BKPM;</li>
			<li>Direktur Lalu Lintas Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Izin Tinggal Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Pengawasan dan Penindakan Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Sistem dan Teknologi Informasi Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Pengendalian Penggunaan Tenaga Kerja Asing, Kementerian Ketenagakerjaan;</li>
			';
		
				foreach($data as $p) {
				if($p['pma_pmd'] == 'Perusahaan PMA' ){
					$table_html.='
					<li>Direksi '.$p['nama_pt'].'.</li>
					';
			
				}else if($p['pma_pmd'] == 'Perusahaan PMDN') {

					
						$table_html.='
						<li>Direksi '.$p['nama_pt'].'.</li>
						';
				}else if($p['pma_pmd'] == 'Yayasan') {

					
						$table_html.='
						<li>Ketua '.$p['nama_pt'].'.</li>
						';

				} else {

					$table_html.='
					<li>Chief of Representative '.$p['nama_pt'].'. </li>
				';
			
			}
			}
		
		

$table_html .='
		</ol>
		                     
		
';
			}
		}
	} else {
		$table_html = '
		<style>
		ol {
		  display: block;
		  list-style-type: decimal;
		  margin-top: 1em;
		  margin-bottom: 1em;
		  margin-left: 0;
		  margin-right: 0;
		  padding-left: 20px;
		}
		</style>

	
			<p>&nbsp;</p>';
			foreach($data as $p) {
				$table_html .= '<p style="text-align:right">'.$p['username'].'</p>
				<p></p>
				<p></p>
				<p></p>
			<table  style="padding-left:10px;padding-right:10px;" border="1">
		<tbody>
		<tr>
		<td >
	
		<p>Yth. Bapak Deputi Bidang Pengembangan Iklim PM</p>
		<p>&nbsp;</p>
	
		<p style="text-align:justify; margin-left:20px;">Terlampir kami sampaikan draft jawaban surat kepada ';
		foreach($data as $p) {
			if($p['tujuan'] == 'Imigrasi'){

		$table_html.='
		 Direktur Jenderal Imigrasi, Kementerian
		Hukum dan HAM';
			} else {

				$table_html.='
		Direktur Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja, Kementerian
		Ketenagakerjaan dan Direktur Jenderal Imigrasi, Kementerian
		Hukum dan HAM';
			}
		}
		$table_html .=' untuk permohonan masuk ke Indonesia bagi '; 
		$i = 0;
		$len = count($dataz);
		$printedSeasons = [];
	
		
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}

		
		$table_html .='
		yang di ajukan  '.$p['nama_pt'].'.</p>
		<p>&nbsp;</p>
		<p>Mohon koreksi dan arahan lebih lanjut</p>
		<p><strong>&nbsp;</strong></p>
		<p><strong>&nbsp;</strong></p>
		
	
		<p>Terima kasih dan salam hormat,</p>
		<p >Suhartono</p>
		<p>&nbsp;</p>
	
		</td>
		</tr>
		</tbody>
		</table>
				<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
				<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>		<p>&nbsp;</p>
<p>&nbsp;</p>			
<BR>
<BR>

		<table width="0">
			<tbody>
				<tr>
					<td width="78">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nomor</p>
					</td>
					<td width="19">
						<p>:</p>
					</td>
					<td width="230">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/A.1/2021</p>
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="150">
						<p>Jakarta,</p>
					</td>
				</tr>
				<tr>
					<td width="78">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lampiran</p>
					</td>
					<td width="19">
						<p>:</p>
					</td>
					<td width="300">
						<p>1 (satu) berkas</p>
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="195">
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
				<td width="78">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Perihal</p>
					</td>
					<td width="19">
						<p>:</p>
					</td>
					<td width="200">
					<p sgyle="text-align-justify">Permohonan Masuk ke Indonesia untuk 
						'; 
						$i = 0;
						$len = count($dataz);
						$printedSeasons = [];
					
					
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}
				
				$table_html .='
				'.$p['nama_pt'].'
				';
						$table_html .='</p>
					</td>
					<td width="40">
						<p><strong>&nbsp;</strong></p>
					</td>
					<td width="195">
						<p><strong>&nbsp;</strong></p>
					</td>
				</tr>
				<tr>
					<td width="78">&nbsp;</td>
					<td width="19">
						<p>&nbsp;</p>
					</td>
					<td width="300">
						<p>&nbsp;</p>
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="195">
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td colspan="3" width="397">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kepada Yth.</p>
						
					</td>
					<td width="40">
						<p>&nbsp;</p>
					</td>
					<td width="195">
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td colspan="5" width="632">
					';
					foreach($data as $p) {
						if($p['tujuan'] == 'Imigrasi'){
							$pdf->SetAutoPageBreak(true, 39);
						$table_html .='
						<p style="text-align: justify;  margin-left: 0; font-family:calibriz; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Direktur Jenderal Imigrasi, Kementerian
						Hukum dan HAM</p><br>
				
				
					';
		
		} else {
			$pdf->SetAutoPageBreak(true, 33);
					$table_html .='
						<ol style="text-align: justify;  margin-left: 0; font-family:calibriz;">
						
				<li style="text-align: justify;  margin-left: 0; font-family:calibriz; ">Direktur Jenderal Imigrasi, Kementerian
				Hukum dan HAM</li>
				<li style="text-align: justify; font-family:calibriz;">Direktur Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja,<br>Kementerian
				Ketenagakerjaan.</li><br>
			</ol>
			';
		}
	
					}
		$table_html .='
					</td>
				</tr>
				<tr>
					<td colspan="4" width="632">
						
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;di - &nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jakarta	<br></p>
					</td>
				</tr>
			
		</table>
	
		<table style="padding-left:20px;padding-right:26px;">
		<tr>
		<td width="568">
		<p style="text-align: justify;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan surat dari '.$p['nama_pt'].' '.$p['no_surat'].' tanggal '.tgl_indo($p['tgl_surat']).' perihal sebagaimana tersebut pada pokok surat, dengan ini kami sampaikan
		hal-hal sebagai berikut:</p>
		</td>
	</tr>

		  </table>

		<ol style="text-align: justify;  margin-left: 0; ">
	
			<li style="text-align: justify;  margin-left: 0; ">'.$p['nama_pt'].' merupakan  '.$p['pma_pmd'].' dengan NIB '.$p['nib'].' bergerak di bidang usaha
			'.$p['bidang_usaha'].' yang berlokasi di '.$p['lokasi'].' dengan ';
			foreach($data as $p) {
				if($p['pma_pmd'] == 'Perusahaan PMA' | $p['pma_pmd'] == 'Perusahaan PMDN'){
			$table_html.='
			total investasi sebesar Rp.';
			if ($p["nilai_investasi"]  == 0) { 
				$table_html.='
				total investasi sebesar Rp.'.$p["nilai_investasi"].' ';
} else if ($p["nilai_investasi"]  < 1000000000) { 
	$table_html.=''.rupiah($p["nilai_investasi"]/1000000).' juta ';
						} else if ($p["nilai_investasi"]  < 1000000000000) { 
				$table_html.=''.rupiah($p["nilai_investasi"]/1000000000).' Miliar ';
	} else if ($p["nilai_investasi"] < 1000000000000000) { 
		$table_html.=''.rupiah($p["nilai_investasi"]/1000000000000).' Triliun ';
				 } 
				 $table_html.='dan
			';
		
				} else {

					$table_html.='
					';
				
				}
			
			
			
			
			$table_html .='
				menyerap Tenaga Kerja Indonesia sebanyak '.number($p['tki']).' orang.</li><br>
			<li style="text-align: justify;">Dalam rangka '.$p['alasan_tka'].', '.$p['nama_pt'].'
				berencana akan mendatangkan '.$p['jumlah_tka'].'';
		
				if($p['rptka'] == ''){
							$table_html .=''.$p['rptka'].'';
		} else {
							$table_html .=' '.$p['rptka'].'';
		}
				
				
				
				$table_html .=''.$p['rptka'].', dengan data Terlampir.</li>
		</ol>
	
		
		
		<ol start="3">
			<li style="text-align: justify;">Mengacu pada Surat Edaran Satuan Tugas Penanganan Covid-19 Nomor 8 Tahun 2021
				tentang Protokol Kesehatan Perjalanan Internasional Pada Masa Pandemi <em >Corona Virus Disease</em> 2019
				(Covid-19) pada butir F angka 2 point a, ';
				foreach($data as $p) {
					if($p['pma_pmd'] == 'Yayasan') {
						$table_html.='Yayasan';
			
					} else {
					$table_html .= 'Perusahaan';
					}
				}
				$table_html.='
				yang akan mendatangkan ';
				
				$i = 0;
				$len = count($dataz);
				$printedSeasons = [];
			
		
				foreach($dataz as $key => $b) {
		
					if (!in_array($b['jenis_tka'], $printedSeasons)) {
						$printedSeasons[] = $b['jenis_tka'];
					
							if ($key === array_key_first($dataz)) {
							
								$table_html .='
								 '.$b['jenis_tka'].'';
							
							}  else if ($key === array_key_last($dataz)){
								
				
					
								$table_html .='
								dan '.$b['jenis_tka'].'';
					
							} else {
						
									$table_html .='dan '.$b['jenis_tka'].'';
								
								}
							$i++;
			
				
						}	
				
				}
		
				
				$table_html .=' telah memenuhi ketentuan
				dalam Peraturan Menteri Hukum dan HAM Nomor 26 Tahun 2020 tentang Visa dan Izin Tinggal Dalam Masa Adaptasi
				Kebiasaan Baru. ';
				foreach($data as $p) {
					if($p['pma_pmd'] == 'Yayasan') {
						$table_html.='Yayasan';
			
					} else {
					$table_html .= 'Perusahaan';
					}
				}
				$table_html.='
				bersedia untuk memenuhi segala persyaratan dan melakukan RT-PCR Covid-19, baik sebelum kedatangan ke Indonesia
				maupun masuk ke Indonesia dan membiayai karantina mandiri dan/atau perawatan di rumah sakit serta mengikuti
				seluruh protokol kesehatan yang ditetapkan oleh pemerintah'; 
				$i = 0;
				$len = count($dataz);
				$printedSeasons = [];
			
				$lena = count($dataz);
				
				
				foreach($dataz as $key => $p) {
				
					if (!in_array($p['negara'], $printedSeasons)) {
						$printedSeasons[] = $p['negara'];
					
				
					if ($p['id_negara'] == 1 | $p['id_negara'] == 2 | $p['id_negara'] == 3 ){
						$jumlah =	count($p);

					
					
					
						
					
						if ($i === 1) {
							$table_html.='
							dengan memperhatikan perjanjian bilateral Travel Corridor Arrangement (TCA) antara Indonesia dan  
							
							'; 
					
						}
					} else{

						if ($i === 0) {
							$table_html.='
							dengan memperhatikan perjanjian bilateral Travel Corridor Arrangement (TCA) antara Indonesia dan  
							
							'; 
					
					}
				
						$i++;
						
					
						
			
					}
				if($p['id_negara'] == 1 ){
					if ($key === array_key_first($dataz)) {
						$table_html .=' '.$p['negara'].'';
					}  else if ($key === array_key_last($dataz)){
						$table_html .=', '.$p['negara'].'';

					} else {

						$table_html .=', '.$p['negara'].'';
					}

					
			
						
					
					// …
					
	
					
			
			
				
			
				}
				else if($p['id_negara'] == 2 ){
					if ($key === array_key_first($dataz)) {
						$table_html .=' '.$p['negara'].'';
					}  else if ($key === array_key_last($dataz)){
						$table_html .=', '.$p['negara'].'';

					} else {

						$table_html .=', '.$p['negara'].'';
					}

			
						
					
					}
					else if($p['id_negara'] == 3 ){
						if ($key === array_key_first($dataz)) {
							$table_html .=' '.$p['negara'].'';
						}  else if ($key === array_key_last($dataz)){
							$table_html .=', '.$p['negara'].'';
	
						} else {
	
							$table_html .=', '.$p['negara'].'';
						}
	
					
		
						
						}
						// else if($p['id_negara'] == 4 ){
						// 	if ($key === array_key_first($dataz)) {
						// 		$table_html .=' '.$p['negara'].',';
						// 	}  else if ($key === array_key_last($dataz)){
						// 		$table_html .=' '.$p['negara'].'';
		
						// 	} else {
		
						// 		$table_html .=' '.$p['negara'].',';
						// 	}
		
						
			
							
						// 	}

							$i++;
			
					
					
			
										} 
					
				}				
				$table_html .='. </li><br>
			<li style="text-align: justify;">Berdasarkan hal tersebut di atas, kami mohon kiranya';
			
			foreach($data as $p) {
				if($p['tujuan'] == 'Imigrasi'){
				$table_html .='
				Direktorat Jenderal Imigrasi, Kementerian Hukum dan HAM berkenan
				memberikan dukungan atas permohonan Izin masuk ke Indonesia bagi ';
				$i = 0;
				$len = count($dataz);
				$printedSeasons = [];
			
			
		foreach($dataz as $key => $b) {
		
			if (!in_array($b['jenis_tka'], $printedSeasons)) {
				$printedSeasons[] = $b['jenis_tka'];
			
					if ($key === array_key_first($dataz)) {
					
						$table_html .='
						 '.$b['jenis_tka'].'';
					
					}  else if ($key === array_key_last($dataz)){
						
		
			
						$table_html .='
						dan '.$b['jenis_tka'].'';
			
					} else {
				
							$table_html .='dan '.$b['jenis_tka'].'';
						
						}
					$i++;
	
		
				}	
		
		}
		

} else {

			$table_html .='
			Direktorat Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja, Kementerian Ketenagakerjaan berkenan menerbitkan ';
			if($p['rptka']== ''){
				$table_html.='RPTKA dan';
			} else {
				$table_html.='';
			}
			$table_html.=' notifikasi penggunaan TKA  serta Direktorat Jenderal Imigrasi, Kementerian Hukum dan HAM berkenan
			memberikan dukungan atas permohonan Izin masuk ke Indonesia bagi ';
			$i = 0;
			$len = count($dataz);
			$printedSeasons = [];
		
		
			foreach($dataz as $key => $b) {
		
				if (!in_array($b['jenis_tka'], $printedSeasons)) {
					$printedSeasons[] = $b['jenis_tka'];
				
						if ($key === array_key_first($dataz)) {
						
							$table_html .='
							 '.$b['jenis_tka'].'';
						
						}  else if ($key === array_key_last($dataz)){
							
			
				
							$table_html .='
							dan '.$b['jenis_tka'].'';
				
						} else {
					
								$table_html .='dan '.$b['jenis_tka'].'';
							
							}
						$i++;
		
			
					}	
			
			}
	
		
}
				
			
			

				}
		
			
		$table_html.=' yang akan diajukan oleh '.$p['nama_pt'].' sesuai dengan Peraturan Perundang-undangan yang berlaku.</li>
					
	</ol>
	<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Demikian atas perhatian dan kerjasama yang baik, disampaikan terima kasih.</p>
		<p></p>
		<p style="text-align: center;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
			a.n. Menteri Investasi / Kepala Badan Koordinasi Penanaman Modal 
			<br>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;  
			Deputi Bidang Pengembangan Iklim Penanaman Modal,</p>

		<p style="text-align: center;">&nbsp;</p>

		<p></p>
		<p style="text-align: center;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			Yuliot</p>
		<p></p>
		<p>  &nbsp; &nbsp; &nbsp; Tembusan Yth. :
		<ol>
			<li>Menteri Investasi / Kepala Badan Koordinasi Penanaman Modal;</li>
		
			';
		
				$table_html .='
			
		
		
			';
				} 
				foreach ($data as $p) {
					if($p['tujuan'] == 'Imigrasi'){
				$table_html .='
				<li style="text-align:justify">Direktur Jenderal Pembinaan Penempatan Tenaga Kerja dan Perluasan Kesempatan Kerja, Kementerian
				Ketenagakerjaan;</li>
				';
				
				} else {
					$table_html .='
					';
				}
					
	

				
				$table_html .='
				<li>Direktur Jenderal Pembinaan Pengawasan Ketenagakerjaan dan Keselamatan dan Kesehatan Kerja, Kementerian
				Ketenagakerjaan;</li>
			<li>Sekretaris Utama, Kementerian Investasi/BKPM;</li>
			<li>Deputi Bidang Pelayanan Penanaman Modal, Kementerian Investasi/BKPM;</li>
			<li>Direktur Lalu Lintas Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Izin Tinggal Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Pengawasan dan Penindakan Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Sistem dan Teknologi Informasi Keimigrasian, Kementerian Hukum dan HAM;</li>
			<li>Direktur Pengendalian Penggunaan Tenaga Kerja Asing, Kementerian Ketenagakerjaan;</li>
			';
		
				foreach($data as $p) {
				if($p['pma_pmd'] == 'Perusahaan PMA' ){
					$table_html.='
					<li>Direksi '.$p['nama_pt'].'.</li>
					';
			
				}else if($p['pma_pmd'] == 'Perusahaan PMDN') {

					
						$table_html.='
						<li>Direksi '.$p['nama_pt'].'.</li>
						';
						
				}else if($p['pma_pmd'] == 'Yayasan') {

					
						$table_html.='
						<li>Ketua '.$p['nama_pt'].'.</li>
						';
				
		
				} else {

					$table_html.='
					<li>Chief of Representative '.$p['nama_pt'].'. </li>
				';
			
			}
			}
		
	}

		}

	}
		$pdf->writeHTML($table_html, true, false, false, false, '');

		ob_clean();
		$pdf->Output('Laporan Konsumen_' . date('YmdHis') . '.pdf', 'I');
	}



	//--------------------------------------------------------------------------
	public function edit_negara($id){

		$data['title'] = 'edit perusahaan';
		$tahun= 2021;
		$data['perusahaaan'] = $this->dashboard_model->get_perusahaan_edit($id);
		$data['jenis'] = $this->dashboard_model->get_jenis_tka($id);
		$data['negara'] = $this->dashboard_model->get_negara_edit($id);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/edit_negara', $data);

    	$this->load->view('admin/includes/_footer');

	}
	public function index_1(){

		$data['all_users'] = $this->dashboard_model->get_all_users();

		$data['active_users'] = $this->dashboard_model->get_active_users();

		$data['deactive_users'] = $this->dashboard_model->get_deactive_users();

		$data['title'] = 'Dashboard';

		$this->load->view('admin/includes/_header', $data);

    	$this->load->view('admin/dashboard/laporan', $data);

    	$this->load->view('admin/includes/_footer');

	}



	//--------------------------------------------------------------------------

	public function index_2(){

		$data['title'] = 'Dashboard';


		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/index2');

    	$this->load->view('admin/includes/_footer');

	}
    public function edit_isian($id,$id_tka){

		$data['title'] = 'Tenaga Ahli';
		$tahun= 2021;
	
		$data['tka'] = 	$this->dashboard_model->get_tka($id_tka);;
		$data['perusahaaan'] = $this->dashboard_model->get_perusahaan($id);
		$data['jenis'] = $this->dashboard_model->get_jenis_tka($id);
		$data['pendukung'] = $this->dashboard_model->get_pendukung_edit($id);
		$data['negara'] = $this->dashboard_model->get_negara($id);
		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/edit_isian', $data);

    	$this->load->view('admin/includes/_footer');

	}

		public function update_isian($id,$id_tka){
	
		// Ambil data yang dikirim dari form
		$perusahaan = $_POST['perusahaan']; // Ambil data nis dan masukkan ke variabel nis
		$jenis = $_POST['jenis']; // Ambil data nis dan masukkan ke variabel nis
	
		$alasan = $_POST['alasan']; // Ambil data nis dan masukkan ke variabel nis
		$rptka = $_POST['rptka']; // Ambil data nis dan masukkan ke variabel nis
	
		$tujuan = $_POST['tujuan']; // Ambil data nis dan masukkan ke variabel nis
		$jumlah = $_POST['jumlah']; // Ambil data nis dan masukkan ke variabel nis
		$tgl_surat= $_POST['date']; // Ambil data nis dan masukkan ke variabel nis
		$jabatan = $_POST['jabatan']; // Ambil data nis dan masukkan ke variabel nis
		$negara = $_POST['negara']; // Ambil data nis dan masukkan ke variabel nis
		$nis = $_POST['nis']; // Ambil data nis dan masukkan ke variabel nis\
		$surat = $_POST['surat']; // Ambil data nis dan masukkan ke variabel nis
		$nama = $_POST['nama']; // Ambil data nama dan masukkan ke variabel nama
		$telp = $_POST['telp']; // Ambil data telp dan masukkan ke variabel telp
		$id_tkaz = $_POST['id_tka']; // Ambil data telp dan masukkan ke variabel telp

		$index = 0; // Set index array awal dengan 0
		$datat = array();
		
		
		$index = 0; // Set index array awal dengan 0
		foreach($nis as $datanis){ // Kita buat perulangan berdasarkan nis sampai data terakhir
		  array_push($datat, array(
		
			'id'=>$id_tkaz[$index],
			'id_negara'=>$negara[$index],
			'id_jenis'=>$jenis[$index],
			'nama_tka'=>$nama[$index],  // Ambil dan set data nama sesuai index array dari $index
			'passpor'=>$telp[$index],  // Ambil dan set data telepon sesuai index array dari $index
			'jabatan'=>$jabatan[$index],  // Ambil dan set data alamat sesuai index array dari $index
		
		  ));
		  
		  $index++;
		}

	

	
		$dataz= 	array(
			
		
			  'id_perusahaan'=>$perusahaan,  // Ambil dan set data nama sesuai index array dari $index
			  'no_surat'=>$surat,  // Ambil dan set data nama sesuai index array dari $index
			  'alasan_tka'=>$alasan, 
			  'rptka'=>$rptka, // Ambil dan set data nama sesuai index array dari $index
			  'jumlah_tka'=>$jumlah,  // Ambil dan set data nama sesuai index array dari $index
			  'tgl_surat'=>$tgl_surat,  // Ambil dan set data nama sesuai index array dari $index
			  'tgl_entry'=> date('Y-m-d'), // Ambil data nis dan masukkan ke variabel nis
			  // Ambil dan set data nama sesuai index array dari $index
			  'tujuan'=>$tujuan ,  // Ambil dan set data alamat sesuai index array dari $index $this->session->userdata('name');
		
			);
			
		

	    
	
			$sql = $this->db->update_batch('ci_tka', $datat, 'id' ); 
			$sql = $this->db->where('id_surat', $id); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)
			$sql = $this->db->update('ci_surat_dukungan', $dataz  ); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)
		
				
  	redirect('admin/dashboard/ta_index');
	 }

	//--------------------------------------------------------------------------

	public function index_3(){

		$data['title'] = 'Dashboard';

		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/index3');

    	$this->load->view('admin/includes/_footer');

	}


}
?>
